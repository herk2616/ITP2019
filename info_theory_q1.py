"""
Question 1 of Info_theory exam
Calculate character probability
"""

import math

try:
    FILE = open("test.txt", "r")
    TEXT = FILE.read()
    FILE.close()
except FileNotFoundError:
    print("No such file")

# Dictionary
DIC = {}

# Find sum of characters
SUM = 0
for ONE in TEXT:
    if 48 < ord(ONE) < 123:
        SUM = SUM+1

# Loop characters
for ONE in range(48, 123):
    # Reset count
    COUNT = 0
    # Loop text
    for C in TEXT:
        # If characters match
        if chr(ONE) == C:
            # Count them
            COUNT = COUNT+1
    DIC[chr(ONE)] = COUNT
    # If character in in file
    if COUNT != 0:
        # Calculate probability
        PROB = COUNT/SUM
        # Calculate entropy
        ENTR = -1 * PROB * math.log2(PROB)
        print("Char = ", chr(ONE), "\tProb = ",
              PROB, "\tEntr = ", ENTR, end='\n')
