"""
Question 3 of Info_theory exam
Fixed sized code for single character
"""

try:
    FILE = open("test.txt", "r")
    TEXT = FILE.read()
    FILE.close()
except FileNotFoundError:
    print("No such file")

# Find sum of characters
SUM = 0
for ONE in TEXT:
    if 48 < ord(ONE) < 123:
        SUM = SUM+1

# Init dictionary
CODE = {}
# Loop all characters and assign a 8 bit binary num
for ONE in range(48, 123):
    # convert to bin(), cut first 2 chars [2:], fill with 0s
    CODE[chr(ONE)] = bin(ONE - 48)[2:].zfill(8)
    # CODE[chr(ONE)] = chr(ONE - 48)

# Encode text
for C in TEXT:
    try:
        print(CODE[C], end='')
    except KeyError:
        print(end='')
