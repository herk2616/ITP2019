"""
Question 6 of Info_theory exam
Compression with header for single character
"""

import math

try:
    FILE = open("test.txt", "r")
    TEXT = FILE.read()
    FILE.close()
except FileNotFoundError:
    print("No such file")

# Flag
VERBOSE = 1
# Dictionary
DIC = {}

# Find sum of characters
SUM = 0
for ONE in TEXT:
    if 48 < ord(ONE) < 123:
        SUM = SUM+1

# Loop characters
for ONE in range(48, 123):
    # Reset count
    COUNT = 0
    # Loop text
    for C in TEXT:
        # If characters mach
        if chr(ONE) == C:
            # Count them
            COUNT = COUNT+1
    # Append to dictionary [character] -> count
    if COUNT != 0:
        DIC[chr(ONE)] = COUNT
        if VERBOSE == 1:
            print(chr(ONE), "  : ", COUNT)

# Make a probbability dictionary
PROB = {}
for ONE in DIC:
    PROB[ONE] = DIC[ONE]/SUM

# Find q*i
QI = {}
for ONE in PROB:
    QI[ONE] = math.ceil( 256 * PROB[ONE]) / 256

# Find Fixed q*i
QIF = {}
for ONE in QI:
    QIF[ONE] = QI[ONE]/len(QI)

# Make a sorted list
SPROB = sorted(QIF.items(), key=lambda t: t[1])

# Make an additive dictionary
APROB = {}
APROB[SPROB[0][0]] = 0
for i in range(0, len(SPROB)-1):
    # Second character is first + PROB(of second)
    APROB[SPROB[i+1][0]] = SPROB[i][1] + APROB[SPROB[i][0]]

# Make a dictionary that FPROB(x) = APROB(x) + PROB(x)/2
FPROB = {}
for i in range(0, len(SPROB)):
    FPROB[SPROB[i][0]] = APROB[SPROB[i][0]] + (SPROB[i][1]/2)

# Make the dictionary that links character to character lenght
IBIT = {}
for i in range(0, len(SPROB)):
    IBIT[SPROB[i][0]] = math.ceil(math.log((1/SPROB[i][1]), 2) + 1)

# Dictionary that liks characters to binary representations
SHAN = {}
# Loop characters in dictionary
for ONE in DIC:
    # Init temp
    TEMP = ""
    # Init float
    FLOAT = FPROB[ONE]
    # Algorythm to convert float num < 1 to bin
    # with lenght of IBIT[ONE]
    for i in range(0, IBIT[ONE]):
        FLOAT = FLOAT * 2
        if FLOAT >= 1:
            FLOAT = FLOAT -1
            TEMP = TEMP + "1"
        else:
            TEMP = TEMP + "0"
    if VERBOSE == 1:
        print(ONE, "  : ", DIC[ONE], " \t  [", IBIT[ONE], "]\t  : ", TEMP)
    # Fill dictionary
    SHAN[ONE] = TEMP
    # Reset temp
    TEMP = ""

# ``Header``
for ONE in SHAN:
    print(SHAN[ONE], end='')

# Encode text
for ONE in TEXT:
    try:
        if VERBOSE == 1:
            print(SHAN[ONE], end=' ')
        else:
            print(SHAN[ONE], end='')
    except KeyError:
        print(end='')
