"""
Question 2 of Info_theory exam
Fixed sized code for pair of characters
"""

try:
    FILE = open("test.txt", "r")
    TEXT = FILE.read()
    FILE.close()
except FileNotFoundError:
    print("No such file")

# Find sum of characters
SUM = 0
for ONE in TEXT:
    if 48 < ord(ONE) < 123:
        SUM = SUM+1

# Init dictionary
CODE = {}
# Init int
COUNT = 0
# Loop first character
for ONE in range(48, 123):
    # Loop second character
    for TWO in range(48, 123):
        # Pair of caracters
        char = chr(ONE) + chr(TWO)
        # Dictionary assaigns chrs -> binary
        CODE[char] = bin(COUNT)[2:].zfill(13)
        # Increment counter
        COUNT = COUNT+1

# Encode text
for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
    CHAR = C1 + C2
    try:
        print(CODE[CHAR], end='')
    except KeyError:
        print(end='')
