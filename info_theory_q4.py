"""
Question 4 of Info_theory exam
Shannon encoding of complex model
"""

import math

try:
    FILE = open("test.txt", "r")
    TEXT = FILE.read()
    FILE.close()
except FileNotFoundError:
    print("No such file")

# Flag
VERBOSE = 1
# Dictionaries
DIC2 = {}
DIC1 = {}

# Find sum of characters
SUM = 0
for ONE in TEXT:
    if 48 < ord(ONE) < 123:
        SUM = SUM+1

# Loop character
for ONE in range(48, 123):
    # Set count
    COUNT = 0
    # Loop text
    for C in TEXT:
        # If character matches
        if chr(ONE) == C:
            COUNT = COUNT+1
    # If character exist in file
    if COUNT != 0:
        DIC1[chr(ONE)] = COUNT

# Loop first character
for ONE in range(48, 123):
    # Loop second character
    for TWO in range(48, 123):
        # Set count
        COUNT = 0
        # Loop text with step of 2
        for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
            # If characters match
            if chr(ONE) == C1 and chr(TWO) == C2:
                # Count them
                COUNT = COUNT+1
        # If characters exist in file
        if COUNT != 0:
            CHAR = chr(ONE) + chr(TWO)
            DIC2[CHAR] = COUNT

def ShannonEncode(DIC):
    #Make a probbability dictionary
    PROB = {}
    for ONE in DIC:
        PROB[ONE] = DIC[ONE]/SUM
    #Make a sorted list
    SPROB = sorted(PROB.items(), key=lambda t: t[1])
    # Make an additive dictionary
    APROB = {}
    APROB[SPROB[0][0]] = 0
    for i in range(0, len(SPROB)-1):
        # Second character is first + PROB(of second)
        APROB[SPROB[i+1][0]] = SPROB[i][1] + APROB[SPROB[i][0]]
    # Make a dictionary that FPROB(x) = APROB(x) + PROB(x)/2
    FPROB = {}
    for i in range(0, len(SPROB)):
        FPROB[SPROB[i][0]] = APROB[SPROB[i][0]] + (SPROB[i][1]/2)
    # Make the dictionary that links character to character lenght
    IBIT = {}
    for i in range(0, len(SPROB)):
        IBIT[SPROB[i][0]] = math.ceil(math.log((1/SPROB[i][1]), 2) + 1)
    # Dictionary that links characters to binary representations
    SHAN = {}
    # Loop characters in dictionary
    for ONE in DIC:
        # Init temp
        TEMP = ""
        # Init float
        FLOAT = FPROB[ONE]
        # Algorythm to convert float num < 1 to bin
        # with lenght of IBIT[ONE]
        for i in range(0, IBIT[ONE]):
            FLOAT = FLOAT * 2
            if FLOAT >= 1:
                FLOAT = FLOAT -1
                TEMP = TEMP + "1"
            else:
                TEMP = TEMP + "0"
        if VERBOSE == 1:
            print(ONE, "  : ", DIC[ONE], " \t  [", IBIT[ONE], "]\t  : ", TEMP)
        # Fill dictionary
        SHAN[ONE] = TEMP
        # Reset temp
        TEMP = ""
    return SHAN

# Encode first dictionary
SHAN1 = ShannonEncode(DIC1)
# Encode second dictionary
SHAN2 = ShannonEncode(DIC2)

# Encode text
for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
    FIRST = C1
    PAIR  = C1+C2
    try:
        if VERBOSE == 1:
            print(SHAN1[FIRST], SHAN2[PAIR], end=' ')
        else:
            print(SHAN1[FIRST], end='')
            print(SHAN2[PAIR], end='')
    except KeyError:
        print(end='')
