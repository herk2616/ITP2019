"""
Question 2 of Info_theory exam
Calculate accumulative probability
"""

import math

try:
    FILE = open("test.txt", "r")
    TEXT = FILE.read()
    FILE.close()
except FileNotFoundError:
    print("No such file")

# Dictionaries
DIC2 = {}
DIC1 = {}

# Find sum of characters
SUM = 0
for ONE in TEXT:
    if 48 < ord(ONE) < 123:
        SUM = SUM+1

# Loop character
for ONE in range(48, 123):
    # Set count
    COUNT = 0
    # Loop text
    for C in TEXT:
        # If character matches
        if chr(ONE) == C:
            COUNT = COUNT+1
    # If character exist in file
    if COUNT != 0:
        DIC1[chr(ONE)] = COUNT
print(DIC1)

# Loop first character
for ONE in range(48, 123):
    # Loop second character
    for TWO in range(48, 123):
        # Set count
        COUNT = 0
        # Loop text with step of 2
        for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
            # If characters match
            if chr(ONE) == C1 and chr(TWO) == C2:
                # Count them
                COUNT = COUNT+1
        # If characters exist in file
        if COUNT != 0:
            CHAR = chr(ONE) + chr(TWO)
            DIC2[CHAR] = COUNT
            print(CHAR, " : ", COUNT)
print(DIC2)


# Init int
ENTRXSUM = 0
for C in DIC1:
    num = DIC1[C]
    prob = num/SUM
    entr = -1 * prob * math.log2(prob)
    ENTRXSUM = ENTRXSUM + entr

# Init int
ENTRDSUM = 0
ENTRSUM = 0
for C in DIC2:
    # Count for pair of digits
    numxy = DIC2[C]
    # Count of first character
    numx = DIC1[C[0]]
    # Count of second character
    numy = DIC1[C[1]]
    # Calculate probabilities
    probxy = numxy/SUM
    probx = numx/SUM
    proby = numy/SUM
    # Calculate entropies
    entrxy = -1 * probxy * math.log(probxy, 2)
    entrx = -1 * probx * math.log(probx, 2)
    entry = -1 * proby * math.log(proby, 2)
    # Calculate posterior entropy
    entrydx = -1 * probxy * math.log(probxy/probx, 2)
    entrxdy = -1 * probxy * math.log(probxy/proby, 2)
    # Find sums
    ENTRSUM = ENTRSUM + entrxy
    ENTRDSUM = ENTRDSUM + entrydx
    print(C, "  : ", probx, probxy)

# Print
print(ENTRXSUM)
print(ENTRSUM)
print(ENTRDSUM)
