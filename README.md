---
title: "Θεωρία Πληροφορίας 1η Εργασία"
author: "Irakleios Kopitas"
date: "04/02/2020"
geometry: margin=3cm
output: pdf_document
header-includes:
- \setlength{\parindent}{1em}
- \setlength{\parskip}{0em}
---


![Unipi](http://www.unipi.gr/unipi/odigos_spoudon/unipilogo.png)

\pagebreak

# Πληροφορίες εκτέλεσης

Υπάρχουν διαφορετικά προγράμματά για κάθε ερώτημα, είναι γραμμένα σε python και
εκτελούνται τρέχοντας:

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```sh
			cd ~/path/to/files
			python ./info_theory_*.py
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

# Απαντήσεις στις ερωτήσεις

## Ερώτημα 1

Οι χαρακτήρες που επέλεξα ξεκινάνε από τον ASCII 48 μέχρι τον 122 δηλαδή το σύνολο:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```C
			0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K
			L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g
			h i j k l m n o p q r s t u v w x y z
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Διαβάζουμε το αρχείο.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			try:
			    FILE = open("test.txt", "r")
			    TEXT = FILE.read()
			    FILE.close()
			except FileNotFoundError:
			    print("No such file")

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και υπολογίζουμε το σύνολο συμβόλων μετρώντας μόνο τους χαρακτήρες από το σύνολο
μας.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Find sum of characters
			SUM = 0
			for ONE in TEXT:
			    if 48 < ord(ONE) < 123:
			        SUM = SUM+1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

Μετράμε πόσες φορές εμφανίζεται ο καθε χαρακτήρας και το αποθηκεύουμε σε ένα
λεξικό
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Loop characters
			for ONE in range(48, 123):
			    # Reset count
			    COUNT = 0
			    # Loop text
			    for C in TEXT:
			        # If characters match
			        if chr(ONE) == C:
			            # Count them
			            COUNT = COUNT+1
			    DIC[chr(ONE)] = COUNT

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Οι χαρακτήρες και οι αντίστοιχες φορές εμφάνισης:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
0 : 3     1 : 14    2 : 8     3 : 6     4 : 4     5 : 2     6 : 1     7 : 7     8 : 3
9 : 3     : : 0     ; : 0     < : 0     = : 0     > : 0     ? : 0     @ : 0     A : 69
B : 19    C : 19    D : 19    E : 21    F : 43    G : 21    H : 67    I : 56    J : 2
K : 7     L : 11    M : 21    N : 7     O : 17    P : 22    Q : 1     R : 20    S : 44
T : 87    U : 1     V : 3     W : 18    X : 0     Y : 14    Z : 1     [ : 0     \ : 0
] : 0     ^ : 0     _ : 0     ` : 0     a : 2376  b : 479   c : 692   d : 1475  e : 3576
f : 635   g : 710   h : 1885  i : 1997  j : 25    k : 235   l : 1242  m : 542   n : 1866
o : 2050  p : 546   q : 24    r : 1712  s : 1752  t : 2645  u : 804   v : 248   w : 746
x : 54    y : 579   z : 23
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και υπολογίζουμε τις πιθανότητες και εντροπίες.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			    # If character in in file
			    if COUNT != 0:
			        # Calculate probability
			        PROB = COUNT/SUM
			        # Calculate entropy
			        ENTR = -1 * PROB * math.log2(PROB)
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

Τα αποτελέσματα είναι:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
Char =  0       Prob =  0.00010143359480659995  Entr =  0.0013457374385447073
Char =  1       Prob =  0.0004733567757641331   Entr =  0.005228123535495501
Char =  2       Prob =  0.0002704895861509332   Entr =  0.003205880261884567
Char =  3       Prob =  0.0002028671896131999   Entr =  0.0024886076874762147
Char =  4       Prob =  0.0001352447930754666   Entr =  0.00173818492401775
Char =  5       Prob =  6.76223965377333e-05    Entr =  0.0009367148585466083
Char =  6       Prob =  3.381119826886665e-05   Entr =  0.0005021686275421708
Char =  7       Prob =  0.00023667838788206654  Entr =  0.002850740155629817
Char =  8       Prob =  0.00010143359480659995  Entr =  0.0013457374385447073
Char =  9       Prob =  0.00010143359480659995  Entr =  0.0013457374385447073
Char =  A       Prob =  0.002332972680551799    Entr =  0.0203986146242638
Char =  B       Prob =  0.0006424127671084663   Entr =  0.006812281054913764
Char =  C       Prob =  0.0006424127671084663   Entr =  0.006812281054913764
Char =  D       Prob =  0.0006424127671084663   Entr =  0.006812281054913764
Char =  E       Prob =  0.0007100351636461996   Entr =  0.007426841358316815
Char =  F       Prob =  0.001453881525561266    Entr =  0.01370410490464773
Char =  G       Prob =  0.0007100351636461996   Entr =  0.007426841358316815
Char =  H       Prob =  0.0022653502840140655   Entr =  0.019903481174867274
Char =  I       Prob =  0.0018934271030565323   Entr =  0.01712563993586894
Char =  J       Prob =  6.76223965377333e-05    Entr =  0.0009367148585466083
Char =  K       Prob =  0.00023667838788206654  Entr =  0.002850740155629817
Char =  L       Prob =  0.00037192318095753316  Entr =  0.004237212091055227
Char =  M       Prob =  0.0007100351636461996   Entr =  0.007426841358316815
Char =  N       Prob =  0.00023667838788206654  Entr =  0.002850740155629817
Char =  O       Prob =  0.000574790370570733    Entr =  0.006187432387000519
Char =  P       Prob =  0.0007438463619150663   Entr =  0.007730577820195389
Char =  Q       Prob =  3.381119826886665e-05   Entr =  0.0005021686275421708
Char =  R       Prob =  0.000676223965377333    Entr =  0.007120781196442981
Char =  S       Prob =  0.0014876927238301326   Entr =  0.013973462916560646
Char =  T       Prob =  0.0029415742493913983   Entr =  0.02473627391849644
Char =  U       Prob =  3.381119826886665e-05   Entr =  0.0005021686275421708
Char =  V       Prob =  0.00010143359480659995  Entr =  0.0013457374385447073
Char =  W       Prob =  0.0006086015688395997   Entr =  0.006501212397937813
Char =  Y       Prob =  0.0004733567757641331   Entr =  0.005228123535495501
Char =  Z       Prob =  3.381119826886665e-05   Entr =  0.0005021686275421708
Char =  a       Prob =  0.08033540708682715     Entr =  0.29224576726907864
Char =  b       Prob =  0.016195563970787124    Entr =  0.09633538457174935
Char =  c       Prob =  0.02339734920205572     Entr =  0.12675539902559368
Char =  d       Prob =  0.049871517446578306    Entr =  0.21572623516870937
Char =  e       Prob =  0.12090884500946714     Entr =  0.3685311639791059
Char =  f       Prob =  0.02147011090073032     Entr =  0.11897718951329114
Char =  g       Prob =  0.02400595077089532     Entr =  0.12916315664033648
Char =  h       Prob =  0.06373410873681364     Entr =  0.2531385288835151
Char =  i       Prob =  0.0675209629429267      Entr =  0.26255666281289936
Char =  j       Prob =  0.0008452799567216662   Entr =  0.008628857129439848
Char =  k       Prob =  0.007945631593183662    Entr =  0.05542572557753676
Char =  l       Prob =  0.04199350824993238     Entr =  0.19206528328708136
Char =  m       Prob =  0.018325669461725723    Entr =  0.10573893479387636
Char =  n       Prob =  0.06309169596970517     Entr =  0.25150911898120143
Char =  o       Prob =  0.06931295645117663     Entr =  0.26690555938188365
Char =  p       Prob =  0.01846091425480119     Entr =  0.10632346071977263
Char =  q       Prob =  0.0008114687584527996   Entr =  0.00833149323299926
Char =  r       Prob =  0.0578847714362997      Entr =  0.23794532895380702
Char =  s       Prob =  0.05923721936705437     Entr =  0.24153101077098982
Char =  t       Prob =  0.08943061942115228     Entr =  0.311494656631613
Char =  u       Prob =  0.027184203408168784    Entr =  0.14138742426814596
Char =  v       Prob =  0.008385177170678928    Entr =  0.057840474317503755
Char =  w       Prob =  0.02522315390857452     Entr =  0.1339124357897375
Char =  x       Prob =  0.001825804706518799    Entr =  0.016609805200340947
Char =  y       Prob =  0.01957668379767379     Entr =  0.11109219497310811
Char =  z       Prob =  0.0007776575601839329   Entr =  0.008032096279381772
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

## Ερώτημα 2
Δημιουργούμε ένα νέο λεξικό που αποτελείτε από όλα τα διψήφια ζευγάρια και
τοποθετούμε μέσα τον αριθμό εμφάνισης τους:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Loop first character
			for ONE in range(48, 123):
			    # Loop second character
			    for TWO in range(48, 123):
			        # Set count
			        COUNT = 0
			        # Loop text with step of 2
			        for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
			            # If characters match
			            if chr(ONE) == C1 and chr(TWO) == C2:
			                # Count them
			                COUNT = COUNT+1
			        # If characters exist in file
			        if COUNT != 0:
			            CHAR = chr(ONE) + chr(TWO)
			            DIC2[CHAR] = COUNT
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Οι χαρακτήρες και οι αντίστοιχες φορές εμφάνισης:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
10  :  1     12  :  2     19  :  1     21  :  1     23  :  1     26  :  1     47  :  1
51  :  1     87  :  1     Aa  :  1     Ac  :  1     Af  :  1     Ah  :  1     Al  :  5
An  :  8     Ar  :  4     As  :  1     At  :  2     Au  :  1     Ba  :  4     Be  :  4
Bo  :  1     Br  :  1     CB  :  1     Ca  :  4     Ce  :  3     Ci  :  2     Co  :  4
Cr  :  1     Da  :  1     De  :  1     Do  :  6     Du  :  1     Ea  :  2     El  :  1
Es  :  1     Et  :  1     Ev  :  1     Ex  :  4     Fa  :  1     Fi  :  2     Fo  :  21
Fr  :  1     Ga  :  3     Go  :  1     Gu  :  5     Ha  :  5     He  :  23    Hi  :  6
Ho  :  2     In  :  1     Is  :  1     It  :  18    Ja  :  1     Ke  :  2     Le  :  1
Li  :  1     Lo  :  4     Lu  :  1     Ma  :  3     Mc  :  2     Me  :  2     Mo  :  1
No  :  2     OK  :  2     Of  :  1     Oh  :  1     Ol  :  1     On  :  1     Pa  :  1
Pl  :  2     Po  :  2     Pr  :  4     Ra  :  2     Re  :  1     Ri  :  1     Ro  :  2
Ru  :  2     Sa  :  1     Se  :  1     Sh  :  3     Si  :  2     Sm  :  1     So  :  5
Sp  :  2     St  :  2     Su  :  1     Sw  :  1     Th  :  44    Ti  :  2     To  :  1
Tw  :  1     Vo  :  1     Wa  :  1     We  :  2     Wh  :  2     Wr  :  1     Ye  :  1
Yo  :  7     aa  :  3     ab  :  35    ac  :  46    ad  :  73    af  :  10    ag  :  27
ai  :  58    aj  :  1     ak  :  13    al  :  86    am  :  18    an  :  203   ap  :  24
ar  :  131   as  :  151   at  :  137   au  :  19    av  :  25    aw  :  10    ax  :  5
ay  :  36    az  :  3     ba  :  33    bb  :  1     be  :  56    bi  :  8     bl  :  39
bo  :  28    br  :  7     bs  :  2     bt  :  1     bu  :  20    by  :  14    cK  :  2
ca  :  41    cc  :  4     ce  :  52    ch  :  81    ci  :  19    ck  :  27    cl  :  11
co  :  35    cr  :  17    cs  :  3     ct  :  25    cu  :  14    cy  :  1     da  :  13
dc  :  1     dd  :  9     de  :  91    dg  :  6     di  :  49    dl  :  11    do  :  29
dr  :  22    ds  :  28    du  :  6     dy  :  2     ea  :  85    eb  :  1     ec  :  49
ed  :  208   ee  :  63    ef  :  18    eg  :  7     eh  :  4     ei  :  12    ej  :  3
ek  :  1     el  :  64    em  :  30    en  :  119   eo  :  13    ep  :  15    eq  :  3
er  :  210   es  :  92    et  :  49    eu  :  1     ev  :  28    ew  :  10    ex  :  14
ey  :  27    fa  :  16    fe  :  26    ff  :  16    fi  :  41    fl  :  14    fo  :  37
fr  :  19    ft  :  22    fu  :  11    ga  :  21    ge  :  43    gg  :  8     gh  :  51
gi  :  14    gl  :  16    gn  :  5     go  :  20    gr  :  24    gs  :  3     gt  :  1
gu  :  20    gy  :  1     ha  :  156   hb  :  1     he  :  431   hi  :  143   hl  :  2
hm  :  1     ho  :  45    hr  :  18    ht  :  35    hu  :  22    hy  :  6     ia  :  12
ib  :  7     ic  :  60    id  :  65    ie  :  30    if  :  23    ig  :  54    ik  :  18
il  :  28    im  :  35    in  :  287   io  :  34    ip  :  12    ir  :  45    is  :  131
it  :  134   iv  :  17    ix  :  1     iz  :  5     je  :  1     jo  :  4     ju  :  7
ke  :  34    ki  :  20    kl  :  2     kn  :  7     ko  :  1     ks  :  4     kw  :  2
ky  :  3     la  :  54    ld  :  48    le  :  94    lf  :  10    lg  :  1     li  :  76
lk  :  1     ll  :  74    lm  :  2     lo  :  52    ls  :  9     lt  :  8     lu  :  7
lv  :  4     lw  :  6     ly  :  89    ma  :  45    mb  :  9     me  :  75    mf  :  1
mi  :  20    ml  :  2     mm  :  4     mo  :  27    mp  :  12    ms  :  12    mu  :  11
my  :  2     na  :  21    nb  :  1     nc  :  31    nd  :  149   ne  :  82    nf  :  4
ng  :  148   nh  :  2     ni  :  34    nk  :  6     nl  :  9     nn  :  6     no  :  67
np  :  2     ns  :  22    nt  :  101   nu  :  4     nv  :  4     nw  :  2     nx  :  2
ny  :  15    oa  :  9     ob  :  20    oc  :  8     od  :  21    of  :  93    og  :  4
oi  :  9     ok  :  17    ol  :  24    om  :  46    on  :  121   oo  :  30    op  :  36
oq  :  1     or  :  119   os  :  31    ot  :  70    ou  :  140   ov  :  19    ow  :  70
ox  :  1     oy  :  3     pa  :  29    pd  :  1     pe  :  55    ph  :  8     pi  :  16
pl  :  32    po  :  17    pp  :  25    pr  :  27    ps  :  4     pt  :  15    pu  :  5
pw  :  1     py  :  3     qu  :  10    ra  :  48    rb  :  5     rc  :  5     rd  :  45
re  :  203   rf  :  3     rg  :  11    ri  :  74    rk  :  13    rl  :  16    rm  :  24
rn  :  15    ro  :  70    rp  :  4     rr  :  17    rs  :  33    rt  :  37    ru  :  17
rv  :  9     rw  :  3     ry  :  30    sa  :  40    sc  :  12    se  :  83    sf  :  1
sh  :  49    si  :  45    sk  :  7     sl  :  24    sm  :  9     sn  :  11    so  :  24
sp  :  17    sq  :  2     sr  :  2     ss  :  28    st  :  114   su  :  25    sw  :  14
sy  :  4     ta  :  40    tc  :  11    te  :  114   tf  :  2     th  :  401   ti  :  65
tl  :  27    tm  :  2     tn  :  2     to  :  131   tr  :  45    ts  :  28    tt  :  31
tu  :  14    tw  :  15    ty  :  18    ua  :  7     ub  :  2     uc  :  6     ud  :  17
ue  :  7     uf  :  1     ug  :  27    ui  :  19    ul  :  48    um  :  17    un  :  42
uo  :  2     up  :  19    ur  :  68    us  :  47    ut  :  52    ux  :  1     uy  :  1
va  :  2     ve  :  84    vi  :  27    vo  :  14    vs  :  1     wa  :  101   we  :  43
wf  :  1     wh  :  60    wi  :  45    wl  :  3     wn  :  21    wo  :  37    wr  :  7
ws  :  5     wu  :  1     xa  :  2     xc  :  1     xe  :  2     xi  :  1     xp  :  7
xt  :  5     xy  :  3     ya  :  1     yb  :  4     ye  :  15    yi  :  7     yo  :  25
yp  :  4     ys  :  6     yt  :  5     yw  :  3     za  :  1     ze  :  6     zi  :  1
zl  :  3     zo  :  2     zy  :  2

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Υπολογίζουμε την εντροπία για ένα ψηφίο.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			for C in DIC1:
			    num = DIC1[C]
			    prob = num/SUM
			    entr = -1 * prob * math.log2(prob)
			    ENTRXSUM = ENTRXSUM + entr
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Υπολογίζουμε την εντροπία για δυο ψηφια και την δεσμευμένη εντροπία.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			for C in DIC2:
			    # Count for pair of digits
			    numxy = DIC2[C]
			    # Count of first character
			    numx = DIC1[C[0]]
			    # Count of second character
			    numy = DIC1[C[1]]
			    # Calculate probabilities
			    probxy = numxy/SUM
			    probx = numx/SUM
			    proby = numy/SUM
			    # Calculate entropies
			    entrxy = -1 * probxy * math.log(probxy, 2)
			    entrx = -1 * probx * math.log(probx, 2)
			    entry = -1 * proby * math.log(proby, 2)
			    # Calculate posterior entropy
			    entrydx = -1 * probxy * math.log(probxy/probx, 2)
			    entrxdy = -1 * probxy * math.log(probxy/proby, 2)
			    # Find sums
			    ENTRSUM = ENTRSUM + entrxy
			    ENTRDSUM = ENTRDSUM + entrydx
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Τα αποτελέσματα είναι:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
> *Από κοινού εντροπία* ` : 4.338245703152427 ` \
> *Εντροπία χαρακτήρα* `  : 3.423386790704306 ` \
> *Υπό συνθήκη εντροπία* `: 1.7402800438213006 `
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

## Ερώτημα 3

### Shannon encoding

Χρησιμοποιώντας το λεξικό από πριν εκτελούμε τις πράξεις για την υλοποίηση.
του αλγορίθμου shannon
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			#Make a probbability dictionary
			PROB = {}
			for ONE in DIC:
			    PROB[ONE] = DIC[ONE]/SUM
			#Make a sorted list
			SPROB = sorted(PROB.items(), key=lambda t: t[1])
			# Make an additive dictionary
			APROB = {}
			APROB[SPROB[0][0]] = 0
			for i in range(0, len(SPROB)-1):
			    # Second character is first + PROB(of second)
			    APROB[SPROB[i+1][0]] = SPROB[i][1] + APROB[SPROB[i][0]]
			# Make a dictionary that FPROB(x) = APROB(x) + PROB(x)/2
			FPROB = {}
			for i in range(0, len(SPROB)):
			    FPROB[SPROB[i][0]] = APROB[SPROB[i][0]] + (SPROB[i][1]/2)
			# Make the dictionary that links character to character lenght
			IBIT = {}
			for i in range(0, len(SPROB)):
			    IBIT[SPROB[i][0]] = math.ceil(math.log((1/SPROB[i][1]), 2) + 1)

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και δημιουργούμε λεξικό που μετατρέπει τους χαρακτήρες στην δυαδική τους αναπαράσταση.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Dictionary that liks characters to binary representations
			SHAN = {}
			# Loop characters in dictionary
			for ONE in DIC:
			    # Init temp
			    TEMP = ""
			    # Init float
			    FLOAT = FPROB[ONE]
			    # Algorythm to convert float num < 1 to bin
			    # with lenght of IBIT[ONE]
			    for i in range(0, IBIT[ONE]):
			        FLOAT = FLOAT * 2
			        if FLOAT >= 1:
			            FLOAT = FLOAT -1
			            TEMP = TEMP + "1"
			        else:
			            TEMP = TEMP + "0"
			    # Fill dictionary
			    SHAN[ONE] = TEMP
			    # Reset temp
			    TEMP = ""
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

Η κωδικοποίηση κάθε χαρακτήρα:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
0  :  000000000001010    U  :  0000000000000101
1  :  0000000010101      V  :  000000000010100
2  :  0000000001111      W  :  000000010001
3  :  00000000001110     Y  :  0000000011001
4  :  00000000001100     Z  :  0000000000000111
5  :  000000000000101    a  :  10111
6  :  0000000000000001   b  :  0000110
7  :  00000000010010     c  :  0010011
8  :  000000000001101    d  :  010011
9  :  000000000010001    e  :  11110
A  :  0000010111         f  :  0010000
B  :  000000010011       g  :  0010110
C  :  000000010110       h  :  10001
D  :  000000011000       i  :  10011
E  :  000000011110       j  :  000000110001
F  :  00000011010        k  :  00000111
G  :  000000100001       l  :  010000
H  :  0000010100         m  :  0001000
I  :  00000100101        n  :  01111
J  :  000000000000111    o  :  10101
K  :  00000000010110     p  :  0001011
L  :  0000000010001      q  :  000000101101
M  :  000000100100       r  :  010110
N  :  00000000011010     s  :  011010
O  :  000000001110       t  :  11010
P  :  000000100111       u  :  0011100
Q  :  0000000000000011   v  :  00001001
R  :  000000011011       w  :  0011001
S  :  00000011101        x  :  00000100001
T  :  0000011001         y  :  0001101
                         z  :  000000101010
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Για την κωδικοποίηση του κείμενου άπλα αντιστοιχούμε κάθε χαρακτήρα στην τιμή του
shannon
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Loop text
			for ONE in TEXT:
			    try:
			        print(SHAN[ONE], end='')
			    except KeyError:
			        print(end='')
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

### Shannon pair encoding

Χρησιμοποιώντας τον προηγούμενο αλγόριθμο και εισάγοντας το λεξικό με τα
ζευγάρια χαρακτήρων έχουμε την κωδικοποίηση shannon για δυο χαρακτήρες:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
10   :  0000000000000001   dd   :  0000010100111       ob   :  000011101010
12   :  000000001100010    de   :  0011101011          oc   :  0000010100011
19   :  0000000000000011   dg   :  00000011100011      od   :  000011110101
21   :  0000000000000101   di   :  00100110001         of   :  0011110001
23   :  0000000000000111   dl   :  0000011001100       og   :  00000010101011
26   :  0000000000001001   do   :  00010110110         oi   :  0000010110100
47   :  0000000000001100   dr   :  000011111011        ok   :  000010101100
51   :  0000000000001110   ds   :  000101011001        ol   :  000100010100
87   :  0000000000010000   du   :  00000011100111      om   :  00100100000
Aa   :  0000000000010010   dy   :  000000010010111     on   :  010000111
Ac   :  0000000000010101   ea   :  0011100010          oo   :  00010111110
Af   :  0000000000010111   eb   :  0000000010000001    op   :  00011100011
Ah   :  0000000000011001   ec   :  00100110100         oq   :  0000000010100000
Al   :  00000010111000     ed   :  010101100           or   :  010000101
An   :  0000010011010      ee   :  0010110100          os   :  00011000100
Ar   :  00000010000101     ef   :  000011000010        ot   :  0011000010
As   :  0000000000011011   eg   :  00000100001001      ou   :  010010101
At   :  000000001100100    eh   :  00000010011001      ov   :  000011010100
Au   :  0000000000011101   ei   :  0000011011111       ow   :  0011000100
Ba   :  00000010000111     ej   :  000000011100010     ox   :  0000000010100010
Be   :  00000010001001     ek   :  0000000010000011    oy   :  000000011101111
Bo   :  0000000000100000   el   :  0010110110          pa   :  00010111000
Br   :  0000000000100010   em   :  00010111010         pd   :  0000000010100101
CB   :  0000000000100100   en   :  010000011           pe   :  00101010100
Ca   :  00000010001100     eo   :  0000011111010       ph   :  0000010100101
Ce   :  000000011001011    ep   :  000010010000        pi   :  000010100011
Ci   :  000000001100111    eq   :  000000011100101     pl   :  00011001001
Co   :  00000010001110     er   :  010110000           po   :  000010101111
Cr   :  0000000000100110   es   :  0011101110          pp   :  000100101001
Da   :  0000000000101000   et   :  00100111000         pr   :  000101000110
De   :  0000000000101011   eu   :  0000000010000110    ps   :  00000010101101
Do   :  00000011011101     ev   :  000101011101        pt   :  000010010100
Du   :  0000000000101101   ew   :  0000011000001       pu   :  00000011001100
Ea   :  000000001101001    ex   :  0000100001001       pw   :  0000000010100111
El   :  0000000000101111   ey   :  000100111110        py   :  000000011110011
Es   :  0000000000110001   fa   :  000010011101        qu   :  0000011000110
Et   :  0000000000110100   fe   :  000100110011        ra   :  00100101010
Ev   :  0000000000110110   ff   :  000010011111        rb   :  00000011001110
Ex   :  00000010010000     fi   :  00011111000         rc   :  00000011010001
Fa   :  0000000000111000   fl   :  0000100001101       rd   :  00100010001
Fi   :  000000001101011    fo   :  00011100101         re   :  010101001
Fo   :  000011101101       fr   :  000011010001        rf   :  000000011110110
Fr   :  0000000000111010   ft   :  000011111110        rg   :  0000011010101
Ga   :  000000011001110    fu   :  0000011001111       ri   :  0011001110
Go   :  0000000000111100   ga   :  000011101111        rk   :  0000011111110
Gu   :  00000010111011     ge   :  00011111110         rl   :  000010100101
Ha   :  00000010111110     gg   :  0000010011110       rm   :  000100011000
He   :  000100000111       gh   :  00100111110         rn   :  000010010110
Hi   :  00000011100000     gi   :  0000100010001       ro   :  0011000111
Ho   :  000000001101101    gl   :  000010100001        rp   :  00000010101111
In   :  0000000000111111   gn   :  00000011000110      rr   :  000010110001
Is   :  0000000001000001   go   :  000011011111        rs   :  00011001101
It   :  000010111101       gr   :  000100010001        rt   :  00011101000
Ja   :  0000000001000011   gs   :  000000011101001     ru   :  000010110011
Ke   :  000000001101111    gt   :  0000000010001000    rv   :  0000010110110
Le   :  0000000001000101   gu   :  000011100001        rw   :  000000011111001
Li   :  0000000001001000   gy   :  0000000010001010    ry   :  00011000000
Lo   :  00000010010010     ha   :  010100010           sa   :  00011110000
Lu   :  0000000001001010   hb   :  0000000010001100    sc   :  0000011101111
Ma   :  000000011010001    he   :  01100000            se   :  0011011100
Mc   :  000000001110010    hi   :  010011000           sf   :  0000000010101001
Me   :  000000001110100    hl   :  000000010011010     sh   :  00100111011
Mo   :  0000000001001100   hm   :  0000000010001110    si   :  00100010100
No   :  000000001110110    ho   :  00100000111         sk   :  00000100011101
OK   :  000000001111000    hr   :  000011000100        sl   :  000100011011
Of   :  0000000001001110   ht   :  00011011100         sm   :  0000010111001
Oh   :  0000000001010000   hu   :  000100000001        sn   :  0000011011001
Ol   :  0000000001010011   hy   :  00000011101010      so   :  000100011110
On   :  0000000001010101   ia   :  0000011100010       sp   :  000010110110
Pa   :  0000000001010111   ib   :  00000100001101      sq   :  000000010110000
Pl   :  000000001111010    ic   :  0010110000          sr   :  000000010110010
Po   :  000000001111101    id   :  0010111001          ss   :  000101100100
Pr   :  00000010010101     ie   :  00010111100         st   :  0011111111
Ra   :  000000001111111    if   :  000100001011        su   :  000100101100
Re   :  0000000001011001   ig   :  00101001101         sw   :  0000100010101
Ri   :  0000000001011011   ik   :  000011000111        sy   :  00000010110001
Ro   :  000000010000001    il   :  000101100001        ta   :  00011110011
Ru   :  000000010000011    im   :  00011011110         tc   :  0000011011100
Sa   :  0000000001011110   in   :  01011010            te   :  0100000011
Se   :  0000000001100000   io   :  00011010000         tf   :  000000010110100
Sh   :  000000011010101    ip   :  0000011100101       th   :  01011101
Si   :  000000010000110    ir   :  00100001010         ti   :  0010111011
Sm   :  0000000001100010   is   :  010001100           tl   :  000101001010
So   :  00000011000001     it   :  010010000           tm   :  000000010110110
Sp   :  000000010001000    iv   :  000010101010        tn   :  000000010111001
St   :  000000010001010    ix   :  0000000010010001    to   :  010001110
Su   :  0000000001100100   iz   :  00000011001001      tr   :  00100010111
Sw   :  0000000001100111   je   :  0000000010010011    ts   :  000101101000
Th   :  00100000100        jo   :  00000010011011      tt   :  00011000111
Ti   :  000000010001100    ju   :  00000100010001      tu   :  0000100011001
To   :  0000000001101001   ke   :  00011010010         tw   :  000010011000
Tw   :  0000000001101011   ki   :  000011100100        ty   :  000011001001
Vo   :  0000000001101101   kl   :  000000010011100     ua   :  00000100100000
Wa   :  0000000001101111   kn   :  00000100010101      ub   :  000000010111011
We   :  000000010001110    ko   :  0000000010010101    uc   :  00000011110111
Wh   :  000000010010001    ks   :  00000010011101      ud   :  000010111000
Wr   :  0000000001110010   kw   :  000000010011110     ue   :  00000100100100
Ye   :  0000000001110100   ky   :  000000011101100     uf   :  0000000010101011
Yo   :  00000100000001     la   :  00101010001         ug   :  000101001101
aa   :  000000011011000    ld   :  00100100111         ui   :  000011010111
ab   :  00011010111        le   :  0011110100          ul   :  00100101101
ac   :  00100011101        lf   :  0000011000100       um   :  000010111011
ad   :  0011001001         lg   :  0000000010010111    un   :  00011111011
af   :  0000010111011      li   :  0011010011          uo   :  000000010111101
ag   :  000100110111       lk   :  0000000010011010    up   :  000011011001
ai   :  0010101110         ll   :  0011001100          ur   :  0010111111
aj   :  0000000001110110   lm   :  000000010100000     us   :  00100100011
ak   :  0000011110011      lo   :  00101000110         ut   :  00101001001
al   :  0011100101         ls   :  0000010101010       ux   :  0000000010101101
am   :  000010111111       lt   :  0000010100000       uy   :  0000000010110000
an   :  010100101          lu   :  00000100011001      va   :  000000010111111
ap   :  000100001110       lv   :  00000010100000      ve   :  0011011111
ar   :  010001010          lw   :  00000011101101      vi   :  000101010001
as   :  010011111          ly   :  0011101000          vo   :  0000100011101
at   :  010010011          ma   :  00100001101         vs   :  0000000010110010
au   :  000011001100       mb   :  0000010101100       wa   :  0011111011
av   :  000100100010       me   :  0011010001          we   :  00100000001
aw   :  0000010111110      mf   :  0000000010011100    wf   :  0000000010110100
ax   :  00000011000011     mi   :  000011100111        wh   :  0010110010
ay   :  00011100000        ml   :  000000010100010     wi   :  00100011010
az   :  000000011011011    mm   :  00000010100010      wl   :  000000011111101
ba   :  00011001011        mo   :  000101000010        wn   :  000011111000
bb   :  0000000001111000   mp   :  0000011101001       wo   :  00011101011
be   :  00101011000        ms   :  0000011101100       wr   :  00000100101000
bi   :  0000010011100      mu   :  0000011010010       ws   :  00000011010100
bl   :  00011101101        my   :  000000010100101     wu   :  0000000010110110
bo   :  000101010101       na   :  000011110010        xa   :  000000011000001
br   :  00000100000101     nb   :  0000000010011110    xc   :  0000000010111001
bs   :  000000010010011    nc   :  00011000010         xe   :  000000011000100
bt   :  0000000001111010   nd   :  010011101           xi   :  0000000010111011
bu   :  000011011100       ne   :  0011011001          xp   :  00000100101100
by   :  0000100000001      nf   :  00000010100100      xt   :  00000011010111
cK   :  000000010010101    ng   :  010011010           xy   :  000000100000000
ca   :  00011110101        nh   :  000000010100111     ya   :  0000000010111101
cc   :  00000010010111     ni   :  00011010100         yb   :  00000010110100
ce   :  00101000010        nk   :  00000011110000      ye   :  000010011010
ch   :  0011010110         nl   :  0000010101111       yi   :  00000100110000
ci   :  000011001111       nn   :  00000011110100      yo   :  000100110000
ck   :  000100111011       no   :  0010111101          yp   :  00000010110110
cl   :  0000011001001      np   :  000000010101001     ys   :  00000011111010
co   :  00011011001        ns   :  000100000100        yt   :  00000011011001
cr   :  000010101000       nt   :  0011111000          yw   :  000000100000011
cs   :  000000011011111    nu   :  00000010100110      za   :  0000000010111111
ct   :  000100100101       nv   :  00000010101000      ze   :  00000011111110
cu   :  0000100000101      nw   :  000000010101011     zi   :  0000000011000001
cy   :  0000000001111101   nx   :  000000010101101     zl   :  000000100000111
da   :  0000011110110      ny   :  000010010010        zo   :  000000011000110
dc   :  0000000001111111   oa   :  0000010110001       zy   :  000000011001000
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

### Fixed size encoding

Για έναν απλό σταθερού μήκους κώδικα άπλα θέτουμε μια τιμή σε κάθε ψηφίο.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			CODE = {}
			# Loop all characters and assign a 8 bit binary num
			for ONE in range(48, 123):
			    # convert to bin(), cut first 2 chars [2:], fill with 0s
			    CODE[chr(ONE)] = bin(ONE - 48)[2:].zfill(8)
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και κωδικοποιούμε το αρχείο όπως πριν
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			for C in TEXT:
			    try:
			        print(CODE[C], end='')
			    except KeyError:
			        print(end='')

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

### Fixed size encoding for pair of characters

Ορίζουμε τιμή σε κάθε ζευγάρι
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			COUNT = 0
			# Loop first character
			for ONE in range(48, 123):
			    # Loop second character
			    for TWO in range(48, 123):
			        # Pair of caracters
			        char = chr(ONE) + chr(TWO)
			        # Dictionary assaigns chrs -> binary
			        CODE[char] = bin(COUNT)[2:].zfill(13)
			        # Increment counter
			        COUNT = COUNT+1
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και κωδικοποιούμε ανά δυο
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
			    CHAR = C1 + C2
			    try:
			        print(CODE[CHAR], end='')
			    except KeyError:
			        print(end='')
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak

### Results

Τα αποτελέσματα από τις κωδικοποιήσεις:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
./test.txt                       : 297840 bits
./info_theory_q3single.py        : 236632 bits
./info_theory_q3pair.py          : 148213 bits
./info_theory_q3_shannon.py      : 172030 bits
./info_theory_q3_shannon_pair.py : 118579 bits

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

## Ερώτημα 4

Για το πολύπλοκο μοντέλο χρησιμοποιούμε τα λεξικά με τον ένα και δυο χαρακτήρες
και φτιάχνουμε αντίστοιχες κωδικοποιήσεις shannon.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Encode first dictionary
			SHAN1 = ShannonEncode(DIC1)
			# Encode second dictionary
			SHAN2 = ShannonEncode(DIC2)
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και κωδικοποιούμε το αρχείο με 1ο χαρακτήρα SHAN1 και δεύτερο χαρακτήρα SHAN2.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Loop file
			for (C1, C2) in zip(TEXT[0::2], TEXT[1::2]):
			    FIRST = C1
			    PAIR  = C1 + C2
			    try:
			        print(SHAN1[FIRST], SHAN2[PAIR], end='')
			    except KeyError:
			        print(end='')
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

## Ερώτημα 5

Τα αποτελέσματα από τις κωδικοποιήσεις στο validate.txt:
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
./validate.txt                   : 106560 bits
./info_theory_q3_shannon.py      : 60648  bits
./info_theory_q3_shannon_pair.py : 41050  bits
./info_theory_q4.py              : 71576  bits

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\pagebreak


## Ερώτημα 6

Δημιουργούμε τις στρογγυλόποιημένες πιθανότητες και μετά τις δίνουμε στον
αλγόριθμο του shannon.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# Make a probbability dictionary
			PROB = {}
			for ONE in DIC:
			    PROB[ONE] = DIC[ONE]/SUM
			# Find q*i
			QI = {}
			for ONE in PROB:
			    QI[ONE] = math.ceil( 256 * PROB[ONE]) / 256
			# Find Fixed q*i
			QIF = {}
			for ONE in QI:
			    QIF[ONE] = QI[ONE]/len(QI)
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Για επικεφαλίδα απλά βαζω με την σειρα τους χαρακτήρες.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			# ``Header``
			for ONE in SHAN:
			    print(SHAN[ONE], end='')
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Και τέλος κωδικοποιούμε το αρχείο.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```Python
			for ONE in TEXT:
			    try:
			        print(SHAN[ONE], end='')
			    except KeyError:
			        print(end='')
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

\

Αντίστοιχα γίνεται η κωδικοποίηση για δυο ψηφία με την διάφορα ότι περνούμε το
κατάλληλο λεξικό και κωδικοποιούμε το αρχείο ανά δυο.
